package assignment2.demo.rest;


import java.net.URI;
import java.util.Collection;

import javax.enterprise.context.ApplicationScoped;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import assignment2.demo.rest.model.car;


/**
 * @author Ken Finnigan
 */
@Path("/")
@ApplicationScoped
public class CategoryResource {

    @PersistenceContext(unitName = "car")
    private EntityManager em;

    @GET
    @Path("/car")
    @Produces(MediaType.APPLICATION_JSON)
    public Collection<car> all() throws Exception {
        return em.createNamedQuery("car.findAll", car.class)
                .getResultList();
    }


    @POST
    @Path("/car")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Transactional
    public Response create(car category) throws Exception {
        if (category.getId() != null) {
            return Response
                    .status(Response.Status.CONFLICT)
                    .entity("Unable to create Category, id was already set.")
                    .build();
        }

        try {
            em.persist(category);
        } catch (Exception e) {
            return Response
                    .serverError()
                    .entity(e.getMessage())
                    .build();
        }
        return Response
                .created(new URI(category.getId().toString()))
                .build();
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/car/{categoryId}")
    public car get(@PathParam("categoryId") Integer categoryId) {
        return em.find(car.class, categoryId);
    }

    @DELETE
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/car/{categoryId}")
    @Transactional
    public Response remove(@PathParam("categoryId") Integer categoryId) throws Exception {
        try {
            car entity = em.find(car.class, categoryId);
            em.remove(entity);
        } catch (Exception e) {
            return Response
                    .serverError()
                    .entity(e.getMessage())
                    .build();
        }

        return Response
                .noContent()
                .build();
    }

    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/car/{categoryId}")
    @Transactional
    public Response update(@PathParam("categoryId") Integer categoryId, car category) throws Exception {
        try {
            car entity = em.find(car.class, categoryId);

            if (null == entity) {
                return Response
                        .status(Response.Status.NOT_FOUND)
                        .entity("Category with id of " + categoryId + " does not exist.")
                        .build();
            }

            em.merge(category);

            return Response
                    .ok(category)
                    .build();
        } catch (Exception e) {
            return Response
                    .serverError()
                    .entity(e.getMessage())
                    .build();
        }
    }
}
