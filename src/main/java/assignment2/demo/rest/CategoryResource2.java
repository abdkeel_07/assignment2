package assignment2.demo.rest;


import java.net.URI;
import java.util.Collection;

import javax.enterprise.context.ApplicationScoped;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import assignment2.demo.rest.model.person;


/**
 * @author Ken Finnigan
 */
@Path("/")
@ApplicationScoped
public class CategoryResource2 {

    @PersistenceContext(unitName = "person")
    private EntityManager em;

    @GET
    @Path("/person")
    @Produces(MediaType.APPLICATION_JSON)
    public Collection<person> all() throws Exception {
        return em.createNamedQuery("person.findAll", person.class)
                .getResultList();
    }


    @POST
    @Path("/person")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Transactional
    public Response create(person category) throws Exception {
        if (category.getId() != null) {
            return Response
                    .status(Response.Status.CONFLICT)
                    .entity("Unable to create Category, id was already set.")
                    .build();
        }

        try {
            em.persist(category);
        } catch (Exception e) {
            return Response
                    .serverError()
                    .entity(e.getMessage())
                    .build();
        }
        return Response
                .created(new URI(category.getId().toString()))
                .build();
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/person/{categoryId}")
    public person get(@PathParam("categoryId") Integer categoryId) {
        return em.find(person.class, categoryId);
    }

    @DELETE
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/person/{categoryId}")
    @Transactional
    public Response remove(@PathParam("categoryId") Integer categoryId) throws Exception {
        try {
            person entity = em.find(person.class, categoryId);
            em.remove(entity);
        } catch (Exception e) {
            return Response
                    .serverError()
                    .entity(e.getMessage())
                    .build();
        }

        return Response
                .noContent()
                .build();
    }

    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/person/{categoryId}")
    @Transactional
    public Response update(@PathParam("categoryId") Integer categoryId, person category) throws Exception {
        try {
            person entity = em.find(person.class, categoryId);

            if (null == entity) {
                return Response
                        .status(Response.Status.NOT_FOUND)
                        .entity("Category with id of " + categoryId + " does not exist.")
                        .build();
            }

            em.merge(category);

            return Response
                    .ok(category)
                    .build();
        } catch (Exception e) {
            return Response
                    .serverError()
                    .entity(e.getMessage())
                    .build();
        }
    }
}
