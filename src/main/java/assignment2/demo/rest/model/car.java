package assignment2.demo.rest.model;


import java.time.LocalDateTime;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Version;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

/**
 * @author Ken Finnigan
 */
@Entity
@Table(name = "car")
@NamedQueries({
        @NamedQuery(name = "car.findAll", query = "SELECT c from car c")
})
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
public class car {

    @Id
    @SequenceGenerator(
            name = "category_sequence",
            allocationSize = 1,
            initialValue = 1020
    )
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "category_sequence")
    @Column(name = "id", updatable = false, nullable = false)
    private Integer id;

    @NotNull
    @Size(min = 3, max = 50)
    @Column(name = "name", length = 50, nullable = false)
    private Integer id;

    private String brand;

    private String type;

    private String licens_plate;

    private String owner;


    @Version


    public Integer getId() {
        return id;
    }

    public String getBrand() {
        return brand;
    }

    public String getType() {
        return type;
    }

    public String getLicens_plate() {
        return licens_plate;
    }
    public String getOwner(){
        return owner;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        car cc = (car) o;
        return Objects.equals(id, cc.id) &&
                Objects.equals(brand, cc.brand) &&
                Objects.equals(type, cc.type) &&
                Objects.equals(licens_plate, cc.licens_plate) &&
                Objects.equals(owner, cc.owner)  ;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, brand, type, licens_plate, owner);
    }
}
